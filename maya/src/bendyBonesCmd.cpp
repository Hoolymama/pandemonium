#include <maya/MPlugArray.h>
#include <maya/MTypeId.h>
#include <maya/MFloatVectorArray.h>
#include "bendyBonesCmd.h"

//	static
void* bendyBonesCmd::creator()
{
	return new bendyBonesCmd();
}

MSyntax bendyBonesCmd::newSyntax()
{
	MSyntax syn;



	//syn.addFlag(kCacheFrameFlag, kCacheFrameFlagL);
	syn.addFlag(kAssumeFlag, kAssumeFlagL);
	syn.addFlag(kJointsFlag, kJointsFlagL);
	
	//syn.addFlag(kForceCacheFlag, kForceCacheFlagL);
	syn.addFlag(kFixFlag, kFixFlagL);
	
	syn.setObjectType(MSyntax::kSelectionList, 0, 1);
	syn.useSelectionAsDefault(true);
		
	// syn.enableQuery(false);
	// syn.enableEdit(true);
	return syn;
		
}


MStatus bendyBonesCmd::doIt( const MArgList& args )
{
	MStatus st;
//	unsigned int counter = 0;
	MString method("bendyBonesCmd::doIt");

	MSelectionList list;
	// MSpace::Space	space=MSpace::kWorld;
	
	MArgDatabase  argData(syntax(), args);

	argData.getObjects(list);
	
	
	MItSelectionList jointIter( list, MFn::kJoint);
	if (! jointIter.isDone()) {
		// if a joint is selected (or named)

		MObject jointNode;				
        jointIter.getDependNode( jointNode );
		

		MFnDependencyNode jointFn( jointNode );
		
		MItDag dagIter( MItDag::kDepthFirst, MFn::kJoint, &st);er;
		st = dagIter.reset(jointNode, MItDag::kDepthFirst, MFn::kJoint  );er;
		MDagPath dagPath;
		MFnDagNode dagNodeFn;
		for ( ; !dagIter.isDone(); dagIter.next() ) {
			MDagPath path;
			st = dagIter.getPath( path );
			int cc = path.childCount();
			if (cc) {
				// loop through children looking for a joint
				bool foundJoint = false;
				MObject childObject;
				for (int i = 0; i < cc; i++) {
					childObject = path.child( i, &st );
					// we only want joints who have joints as children
					if (childObject.hasFn(MFn::kJoint)) {
					 	foundJoint = true;
						break;
					}
				}
				if (foundJoint) {
					appendToResult(dagIter.partialPathName());
				}
			}
		}
		return MS::kSuccess;

	}
	MItSelectionList bendyIter( list, MFn::kPluginDependNode);
	if (bendyIter.isDone()) {
		displayError("Must pick at least one bendy");
		return MS::kUnknownParameter;
	}
	
	MObject  bendyObject;
	bendyIter.getDependNode( bendyObject );
	MFnDependencyNode bendyFn( bendyObject );
	
	if (!(bendyFn.typeId() == bendyBones::id)){
		displayError("Must supply one bendy");
		return MS::kUnknownParameter;
	}
	bendyBones * bendyBonesNode = (bendyBones*)bendyFn.userNode();

/*
	// cache frame
	if (argData.isFlagSet(kCacheFrameFlag)) {
		MString result;
		st = bendyNode->writeFrameCache(result);
		if (st == MS::kSuccess){
			setResult(result);
			return MS::kSuccess;
		} else {
			MGlobal::displayError("NO FILE WRITTEN");
			return MS::kSuccess;
		}
		return MS::kSuccess;
	} 
*/
	// fix
	if (argData.isFlagSet(kFixFlag)) {
		st = bendyBonesNode->fix();
		if (st.error()){
			setResult(0);
		} else {
			setResult(1);
		}
		return MS::kSuccess;
	} 
	
	
	/*
	// force cache
	if (argData.isFlagSet(kForceCacheFlag)) {
		st = bendyNode->forceRestPoseCache();
		if (st.error()){
			setResult(0);
		} else {
			setResult(1);
		}
		return MS::kSuccess;
	} 
	 */

	// assume rest pose
	if (argData.isFlagSet(kAssumeFlag)) {
		setResult(bendyBonesNode->assumePreferredAngle());
		return MS::kSuccess;
	} 

	// recurse to get joints
	if (argData.isFlagSet(kJointsFlag)) {
		MObject	rootJointNode;
		MPlugArray plugArray;	
		MPlug rootPlug = bendyFn.findPlug("rootParentMatrix");
		// MPlug rootPlug(bendyNode, aRootParentMatrix );
		if(rootPlug.connectedTo(plugArray,1,0)) {
			rootJointNode = plugArray[0].node(&st); er;
		} else {
			// no root joint - we've done enough
			return( MS::kSuccess );
		}
	
		MFnDependencyNode jointFn( rootJointNode );

		MItDag dagIter( MItDag::kDepthFirst, MFn::kJoint, &st);er;
		st = dagIter.reset(rootJointNode, MItDag::kDepthFirst, MFn::kJoint  );er;
		MDagPath dagPath;
		MFnDagNode dagNodeFn;
		for ( ; !dagIter.isDone(); dagIter.next() ) {
			MDagPath path;
			st = dagIter.getPath( path );
			int cc = path.childCount();
			if (cc) {
				// loop through children looking for a joint
				bool foundJoint = false;
				MObject childObject;
				for (int i = 0; i < cc; i++) {
					 childObject = path.child( i, &st );
					 if (childObject.hasFn(MFn::kJoint)) {
					 	foundJoint = true;
						break;
					 }
				}
				if (foundJoint) {
					appendToResult(dagIter.partialPathName());
				}
			}
		}
		return MS::kSuccess;
	} 
	return MS::kSuccess;
}

