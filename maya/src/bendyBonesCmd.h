#ifndef	__bendyBonesCmd_H__
#define	__bendyBonesCmd_H__

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxCommand.h>
#include <maya/MItDag.h>
#include <maya/MItSelectionList.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include "bendyBones.h"


#define kAssumeFlag				"-a"
#define kAssumeFlagL 			"-assume"

#define kJointsFlag				"-j"
#define kJointsFlagL 			"-joints"

/*
#define kCacheFrameFlag			"-c"
#define kCacheFrameFlagL 		"-cacheFrame"

#define kForceCacheFlag			"-fc"
#define kForceCacheFlagL 		"-forceCache"
*/


#define kFixFlag				"-fx"
#define kFixFlagL 				"-fix"






/////////////////////////////////////////
//
//	bendyBonesCmd
//		: MPxCommand
//
class bendyBonesCmd : public MPxCommand
/** @dia:pos -52.2,6.6 */
{
	public:
	
	bendyBonesCmd() {}
	virtual ~bendyBonesCmd() {}
	
	MStatus doIt( const MArgList& args );
	
	static void* creator();
	
   static MSyntax      newSyntax();	

};

#endif	//	!__bendyBonesCmd_H__

