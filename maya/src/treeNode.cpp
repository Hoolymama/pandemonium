#include <maya/MString.h>
#include <maya/MMatrix.h>
#include <maya/MQuaternion.h>
#include <maya/MVector.h>
#include <maya/MAnimControl.h>

#include "treeNode.h"


treeNode::treeNode()
	:m_index(0), 
	m_depth(0), 
	m_parent(0), 
	// m_volume(1),
	// m_area(1),
	// m_radius(1),
	// m_vMOI(1),
	m_weight(1),
	m_mass(1),
	m_spring(1),
	m_inertia(1),
	m_conserve(1)
	// m_IRecip(1),
	// m_I(1)
{ }

treeNode::~treeNode(){}
