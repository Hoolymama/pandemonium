/***************************************************************************
bendyBones.cpp  -  description
-------------------
    begin                : Wed Mar 29 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

initializePlugin and uninitializePlugin
***************************************************************************/

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MString.h>

#include "errorMacros.h"

#include "treeData.h"
#include "bendyBones.h"
#include "bendyBonesCmd.h"







MStatus initializePlugin( MObject obj )
{
	// cerr << "Initializing Bendy Bones " << endl;
	MStatus st;
	
	MString method("initializePlugin");

	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	
	//st = plugin.registerData( "treeData", treeData::id, treeData::creator );er;
	
	st = plugin.registerNode( "bendyBones", bendyBones::id, bendyBones::creator, bendyBones::initialize, MPxNode::kLocatorNode);er;
	
	st = plugin.registerCommand( "bendyBonesCmd",bendyBonesCmd::creator ,bendyBonesCmd::newSyntax);er;
			MGlobal::executePythonCommand("import pandemonium;pandemonium.load()");

	//MString errStr;
	//st.perror(errStr);
	//cerr  << "Err Str "<< errStr << endl;
	
	
	// st = plugin.registerNode( "bendyBonesEmitter", bendyBonesEmitter::id, bendyBonesEmitter::creator, bendyBonesEmitter::initialize, MPxNode::kEmitterNode  );er;
	
    //st = plugin.registerUI("bendyBonesMenu createUI", "bendyBonesMenu deleteUI"); er;
	

	return st;
	
}



MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );

	//st = plugin.deregisterNode( bendyBonesEmitter::id );er;
	
	st = plugin.deregisterCommand( "bendyBonesCmd" );er;
	
	st = plugin.deregisterNode( bendyBones::id );er;
	
	//st = plugin.deregisterData( treeData::id );er;
	
	return st;
}


