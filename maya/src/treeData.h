#ifndef _treeData
#define _treeData

#include <maya/MIOStream.h>
#include <vector>
#include <math.h>
#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MDoubleArray.h>
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MDagPath.h>

#include <maya/MMatrix.h>
#include <maya/MFnIkJoint.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MVectorArray.h>
#include <maya/MBoundingBox.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MFnNurbsSurfaceData.h>
#include <maya/MArrayDataBuilder.h>
//#include "bendy.h"
//#include "bendyCache.h"
#include "treeNode.h"

// using namespace std;

// typedef std::vector<MObject*> NURBS_LIST; 

class treeData // : public MPxData
{
public:
//
	treeData();			
	treeData(bool hard);	
	virtual ~treeData();  // destructor

	//virtual void			copy( const MPxData& );
	//treeData& operator=( const treeData & otherData );
	
	//MTypeId                 typeId() const;
	//MString						name() const;
	
	MStatus init();
	
	// pass index by reference so it always gets incremented
	treeNode* build( 
		const MObject & dagNode, 
		const MMatrix & parentMat,
		int & index , 
		int depth,
		const double &spring,
		const double &density, 
		const double &inertia,		 
		const double &conserve ,
		const double &dynWeight
	);
	  
	MStatus updateRestMatrix(
			treeNode * t,
			const MObject & dagNode
	);

   MStatus prepAnimated( treeNode * t, const MObject & dagNode  );
   
   MStatus getJointMat( const MObject & dagNode, MMatrix & mat ) ;
   
	MStatus  updateCoefficients( 
		treeNode * t,
		const MFnIkJoint & jointFn, 
		const double &spring,
		const double &density,
		const double &inertia,
		const double &conserve,
		const double &dynWeight
	);
	
	MStatus  updateCoefficients( 
		// overloaded version for use when there is no joint tree
		treeNode * t,
		const double &spring,
		const double &density,
		const double &inertia,
		const double &conserve,
		const double &dynWeight
	) ;
	
	int size();
   
	void makeEmpty(treeNode * t) ;

	void getFieldData(
		treeNode * t, 
		MVectorArray &r,
		MVectorArray &v,
		MDoubleArray &m,
		int & index
	);

	MStatus fix(treeNode *t);
	
	void storeAppliedForce(treeNode * t, const MVectorArray &appliedForce, int & index);

	void distributeForce(treeNode * t, const double &ratio);

	void distributeForce(treeNode * t);

	void clearForces(treeNode * t);

	void update(  treeNode * t,  const MMatrix &parentMatrix, const double & dt  );
	
	void update( treeNode * t) ;
	
	void setRotations( treeNode * t, MVectorArray & rotations);

	void getLocatorData(
		treeNode * t,
		const MMatrix &parentMatrix,
		const MMatrix &rootParentMatrix,
		bool doSegments ,
		bool doRawForce ,
		bool doDistributedForce ,
		bool doDistributedTorque ,
		bool doSpringTorque ,
		bool doTensionTorque ,
		bool doVel ,
		bool doAcc ,
		bool doPhi ,
		bool doOmega ,
		bool doAlpha ,
		bool doMatrix ,
		MVectorArray & positions,
		MVectorArray & segments,
		MVectorArray & rawForce,
		MVectorArray & distForce,
		MVectorArray & distTorque,
		MVectorArray & springTorque,
		MVectorArray & inertiaTorque,
		MVectorArray & vel,
		MVectorArray & acc,
		MVectorArray & phi,
		MVectorArray & omega,
		MVectorArray & alpha,
		MVectorArray & restMatrixX,
		MVectorArray & restMatrixY,
		MVectorArray & restMatrixZ,
		MVectorArray & matrixX,
		MVectorArray & matrixY,
		MVectorArray & matrixZ
	);


	
	void getEmitterData(
		treeNode * t,
		const MMatrix &parentMatrix,
		MVectorArray & positions,
		MVectorArray & vel,
		MVectorArray & acc,
		MVectorArray & rawForce
	);

	void calcBoundingBox(
		treeNode * t,
		MPoint & minBB,
		MPoint & maxBB
	);

	MMatrix parentMatrix();

	void setParentMatrix(const MMatrix & m);

	void setBB(const MBoundingBox &  bb);

	MBoundingBox getBB();


	treeNode * m_root;


	// static methods and data.
//	static const MString    typeName;
//   static const MTypeId    id;
//	static void* creator();
	
  

private:

	MBoundingBox m_BB;
	MMatrix m_parentMatrix;
	bool m_isSoftLink;		
	int m_size;
} ;

#endif


