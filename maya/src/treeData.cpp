#include "treeData.h"
#include <maya/MFnIkJoint.h>
#include <maya/MFnDagNode.h>

#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MQuaternion.h>
#include <maya/MEulerRotation.h>
#include <maya/MPoint.h>

#include <maya/MFnNurbsSurface.h>
#include <maya/MFnNurbsSurfaceData.h>
#include <maya/MPointArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MPlug.h>
#include "errorMacros.h"


const double PI_X2 = 6.283185306;
const double PI = 3.141592653;
const double PI_OVER_2 = 1.570796;


// 
/////////////////////////////////////////////////////
treeData::treeData() // default  (soft) constructor
:m_root(0),
m_isSoftLink(true)
{}

treeData::treeData(bool hard) // explicit (set soft or hard) constructor
:m_root(0),
m_isSoftLink(!(hard))
{
	if (hard) {
		m_root = new treeNode;
		m_size = 1;
	}
}


// double check the destructor later
treeData::~treeData() {
	if (!(m_isSoftLink)) {
		makeEmpty(m_root); // recursively delete tree
		if ( m_root != 0) {
			delete m_root;  // then delete root
			m_root = 0;
		}
	}
}
/*
void * treeData::creator() { return new treeData; }
*/
MStatus treeData::init(){
	// we don't initialize a soft version
	// cerr << " in init" <<endl;
	if (!(m_isSoftLink)) {
		makeEmpty(m_root);
		m_size = 1;

		// cerr << "setting m_size to 1 : size =; " << size() << endl;
		return MS::kSuccess;
	}
	return MS::kFailure;
}

int treeData::size() {
	return m_size;
}
/////////////////////////////////////////////////////

// The parent for the whole tree
/////////////////////////////////////////////////////
MMatrix treeData::parentMatrix(){
	return m_parentMatrix;
}

void treeData::setParentMatrix(const MMatrix & m){
	
	m_parentMatrix = m;
}
/////////////////////////////////////////////////////



MStatus  treeData::getJointMat( const MObject & dagNode, MMatrix & mat ) {
	MStatus st;
	MFnTransform jointFn(dagNode, &st  );
	if (st.error()) return st; 

	MTransformationMatrix mtmat = jointFn.transformation();
	mat = mtmat.asMatrix();

	return MS::kSuccess; 
}





// MStatus  treeData::getJointMat( const MObject & dagNode, MMatrix & mat ) {
//    MStatus st;
//    MFnIkJoint jointFn(dagNode, &st  );
//    if (st.error()) return st; 

//    // build the matrix which represents the joint in it's current angle
//    /////////////////////////////////////////////////////
//    MTX::RotationOrder rOrder = jointFn.rotationOrder(&st);ert;
//    double3 scale ;
//    st = jointFn.getScale(scale);ert;
//    double3 scaleOrientation ;
//    // st = jointFn.getScaleOrientation(scaleOrientation,rOrder) ;ert;
//     st = jointFn.getScaleOrientation(scaleOrientation,rOrder) ;ert;
//   double3 rotation;
//    // st = jointFn.getRotation(rotation,rOrder); ert;
//   st = jointFn.getRotation(rotation,rOrder); ert;
//    double3 orientation;
//    // st = jointFn.getOrientation(orientation, rOrder);ert;
//   st = jointFn.getOrientation(orientation, rOrder);ert;
//    // changed from translation due to deprecation
//    MVector trans = jointFn.getTranslation(MSpace::kTransform ,&st);ert;

//    MTX mtxmat;
//    MMatrix Smat;
//    MMatrix ROmat;
//    MMatrix Rmat;
//    MMatrix JOmat;
//    MMatrix Tmat;
//    Smat(0,0) = scale[0];
//    Smat(1,1) = scale[1];
//    Smat(2,2) = scale[2];
//    // mtxmat.setRotation(scaleOrientation , rOrder);
//   mtxmat.setRotation(scaleOrientation , rOrder);


//    ROmat = mtxmat.asMatrix();
//    // mtxmat.setRotation(rotation , rOrder);
//    mtxmat.setRotation(rotation , rOrder);


//    Rmat = mtxmat.asMatrix();
//    // mtxmat.setRotation(orientation, rOrder);
//    mtxmat.setRotation(orientation, rOrder);


//    JOmat = mtxmat.asMatrix();
//    Tmat(3,0) = trans.x;
//    Tmat(3,1) = trans.y;
//    Tmat(3,2) = trans.z;
//    mat = Smat*ROmat*Rmat*JOmat*Tmat;
//    return MS::kSuccess; 
// }


/////////////////////////////////////////////////////
// build the tree based on the given joint hierarchy
/////////////////////////////////////////////////////
treeNode* treeData::build(
	const MObject & dagNode,
	const MMatrix & parentMat,
	int & index,
	int depth,
	const double &spring,
	const double &density,
	const double &inertia,
	const double &conserve,
	const double &dynWeight
	) {
	MStatus st;
//	unsigned int counter = 0;
	MString method("treeData::build");

	MMatrix mat;
	st = getJointMat(dagNode, mat);er;

	MFnIkJoint jointFn(dagNode, &st  );


	treeNode * t = 0;

	// check if root node
	////////////////////////////////////////////////////////////////
	MMatrix worldMat = mat * parentMat;
	if (depth){
		t = new treeNode;
		m_size ++;
	} else{
		t = m_root;
		m_size = 1;
		t->m_appliedForce = MVector::zero;
		t->m_distributedForce = MVector::zero;
		t->m_v = MVector::zero;
		t->m_a = MVector::zero;
		t->m_phi = MVector::zero;
		t->m_omega = MVector::zero;
		t->m_alpha = MVector::zero;
	}
	////////////////////////////////////////////////////////////////



	// initialize simulation coefficients
	////////////////////////////////////////////////////////////////



	t->m_mass = density;

	t->m_weight = dynWeight ;

	t->m_inertia = inertia ;
	
	t->m_conserve = conserve ;
	
	t->m_spring = spring ;



	// initialize simulation coefficients
	////////////////////////////////////////////////////////////////
	MPlug plug;
	plug = jointFn.findPlug ( "bendySpring", &st);
	if (st==MS::kSuccess) {
		JPMDBG;
		plug.getValue( t->m_spring );
		cerr << "t->m_spring: " << t->m_spring << endl;
	}

	double jointDensity;
	plug = jointFn.findPlug ( "bendyDensity", &st);
	if (st==MS::kSuccess) {
		JPMDBG;
		plug.getValue( t->m_mass  );
		cerr << "t->m_mass: " << t->m_mass  << endl;
	}


	double jointInertia;
	plug = jointFn.findPlug ( "bendyInertia", &st);
	if (st==MS::kSuccess) {
		JPMDBG;
		plug.getValue(t->m_inertia);
		cerr << "t->m_inertia: " << t->m_inertia << endl;
	}

	double jointConserve;
	plug = jointFn.findPlug ( "bendyConserve", &st);
	if (st==MS::kSuccess) {
		JPMDBG;
		plug.getValue( t->m_conserve  );
		cerr << "t->m_conserve: " << t->m_conserve  << endl;
	}


	double jointDynWeight;
	plug = jointFn.findPlug ( "bendyDynWeight", &st);
	if (st==MS::kSuccess) {
		JPMDBG;
		plug.getValue( t->m_weight );
		cerr << "t->m_weight: " << t->m_weight << endl;
	}



	// t->m_massRecip = 1.0 / t->m_mass;


	st = jointFn.getOrientation(t->m_orientation);er;	
	t->m_orientation.invertIt();
	
	st = jointFn.getScaleOrientation(t->m_scaleOrientation);er;
	t->m_scaleOrientation.invertIt();

	t->m_r = MVector(worldMat(3,0),worldMat(3,1) ,worldMat(3,2));
	t->m_depth = depth;
	t->m_index = index;
	t->m_restMatrix = mat;
	t->m_matrix = mat;
	depth ++;
	index ++;
	////////////////////////////////////////////////////////////////


	int cc = jointFn.childCount();
	MObject child;

	if (cc) {


		// moment of inertia and spring strength
		////////////////////////////////////////////////////////////////
		// t->m_vMOI;
	 //    MPlug vMOIPlug = jointFn.findPlug ( "bendyMOI", &st);
		// if (!(st==MS::kSuccess)) {
		// 	t->m_vMOI = 1.0; 
		// } else {
		// 	vMOIPlug.getValue( t->m_vMOI );
		// }

		// double area;
	 //    MPlug areaPlug = jointFn.findPlug ( "bendyArea", &st);
		// if (!(st==MS::kSuccess)) {
		// 	area = 1.0;
		// } else {
		// 	areaPlug.getValue( area);
		// }

		// t->m_area = area;
		// t->m_spring = t->m_area * spring;

		// t->m_I = density;
		// t->m_IRecip = 1.0 / t->m_I;

		////////////////////////////////////////////////////////////////

		// recursion
		////////////////////////////////////////////////////////////////
		for (int i = 0;i<cc;i++){
			child = jointFn.child(i);
			if (child.hasFn(MFn::kJoint)) {
				treeNode * c = build( child, worldMat, index , depth, spring,  density, inertia, conserve, dynWeight );
				c->m_parent=t;
				t->m_children.push_back(c);
			} 
		}
		////////////////////////////////////////////////////////////////
	}
	return t;
}



MStatus treeData::updateRestMatrix( treeNode * t, const MObject & dagNode ) {


	MStatus st;
	//	unsigned int counter = 0;
	MString method("treeData::updateRestMatrix");
	

	MMatrix mat;
	st = getJointMat(dagNode, mat);er;
	t->m_restMatrix = mat; 

	MFnIkJoint jointFn(dagNode, &st  );

	unsigned cc = jointFn.childCount();
	// get the number of actual joints
	// unsigned jc=0;
	MIntArray jointChildIndices;
	for (unsigned i = 0;i<cc;i++) {
		if (jointFn.child(i).hasFn(MFn::kJoint) ) {
			jointChildIndices.append(i);
		}
	}
	unsigned jc = jointChildIndices.length();
	
	MObject child;
	//if (!(cc == t->m_children.size())) return MS::kFailure;
	if (!(jc == t->m_children.size())) return MS::kFailure;
	
	LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
	int i = 0;
	while (currentChild != t->m_children.end()) {
		//child = jointFn.child(i);
		child = jointFn.child(jointChildIndices[i]);
		
		st = updateRestMatrix( *currentChild, child );
		if (st != MS::kSuccess)  return MS::kFailure;
		currentChild ++; i++;
	}
	
	return MS::kSuccess;
}



MStatus treeData::fix(

	treeNode * t
	) {
	MStatus st;
//	unsigned int counter = 0;
	MString method("treeData::fix");

	// cerr << "in  treeData::fix " <<  endl;

	if (t != 0) {

		t->m_restMatrix = t->m_matrix ;

		//int cc =  t->m_children.size();

		LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
		while (currentChild != t->m_children.end()) {
			st = fix(*currentChild);
			if (st != MS::kSuccess)  return MS::kFailure;
			currentChild ++;
		}
	}
	return MS::kSuccess;
}



/////////////////////////////////////////////////////
MStatus  treeData::updateCoefficients(
	treeNode * t,
	const  MFnIkJoint & jointFn,
	const double &spring,
	const double &density,
	const double &inertia,
	const double &conserve,
	const double &dynWeight
	) {
	MStatus st;
//	unsigned int counter = 0;


	if (!t) return MS::kFailure; // if this pointer is not a tree node then return  0

	t->m_mass =  density;
	t->m_weight = dynWeight ;
	t->m_inertia = inertia ;
	t->m_conserve = conserve ;
	t->m_spring = spring ;


	
	// initialize simulation coefficients
	////////////////////////////////////////////////////////////////
	MPlug plug;
	plug = jointFn.findPlug ( "bendySpring", &st);
	if (st==MS::kSuccess) {
		JPMDBG;

		plug.getValue( t->m_spring );
		cerr << " t->m_spring : " <<  t->m_spring  << endl;
	}

	double jointDensity;
	plug = jointFn.findPlug ( "bendyDensity", &st);
	if (st==MS::kSuccess) {
		JPMDBG;

		plug.getValue( t->m_mass  );
		cerr << " t->m_mass  : " <<  t->m_mass   << endl;
	}


	double jointInertia;
	plug = jointFn.findPlug ( "bendyInertia", &st);
	if (st==MS::kSuccess) {
		JPMDBG;

		plug.getValue( t->m_inertia);
		cerr << " t->m_inertia: " <<  t->m_inertia << endl;
	}

	double jointConserve;
	plug = jointFn.findPlug ( "bendyConserve", &st);
	if (st==MS::kSuccess) {
		JPMDBG;

		plug.getValue( t->m_conserve  );
		cerr << " t->m_conserve  : " <<  t->m_conserve   << endl;
	}


	double jointDynWeight;
	plug = jointFn.findPlug ( "bendyDynWeight", &st);
	if (st==MS::kSuccess) {
		JPMDBG;

		plug.getValue( t->m_weight );
		cerr << " t->m_weight : " <<  t->m_weight  << endl;
	}




	unsigned jc = jointFn.childCount();
	unsigned tc =  unsigned(t->m_children.size());

	if (jc < tc) {
		// cerr << "less joints than tree nodes - something is very wrong" << endl;
		return MS::kFailure;
	}

	MObject child;
	if (jc) {

		LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();

		for (unsigned i = 0; i < jc; i++) {
			// cerr << " in child loop" << endl;
			child = jointFn.child(i);
			MFnIkJoint childFn(child, &st );
			if (st == MS::kSuccess) {  // valid joint
				st = updateCoefficients( *currentChild, childFn, spring, density, inertia, conserve, dynWeight );
				currentChild ++;
			}
		}
	}
	return st;
}
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// MStatus  treeData::updateCoefficients(
// 	// overloaded version for use when there is no joint tree
// 	treeNode * t,
// 	const double &spring,
// 	const double &density,
// 	const double &inertia,
// 	const double &conserve,
// 	const double &dynWeight
// ) {
// 	MStatus st;
// //	unsigned int counter = 0;
// 	MString method("treeData::updateCoefficients");

// 	if (!t) return MS::kFailure; // if this pointer is not a tree node then return  0

// 	//
// 	/////////////////////////////////////////////
// 	t->m_mass = (density*t->m_volume);
// 	t->m_inertia = inertia;
// 	t->m_conserve = conserve;
// 	/////////////////////////////////////////////

// 	unsigned tc =  unsigned(t->m_children.size());

// 	if (tc) {


// 	t->m_spring = t->m_area * spring;
// 	t->m_I = (density*t->m_vMOI);

// 		t->m_IRecip = 1.0/ t->m_I;
// 		/////////////////////////////////////////////


// 		LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();

// 		for (unsigned i = 0; i < tc; i++) {
// 			if (st == MS::kSuccess) {  // valid joint
// 				st = updateCoefficients( *currentChild,  spring,density, inertia, conserve,dynWeight );
// 				currentChild ++;
// 			}
// 		}
// 	}
// 	return st;
// }
/////////////////////////////////////////////////////

void treeData::makeEmpty(treeNode * t){
	if (t != 0) {
		if (t->m_children.size()) {
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				makeEmpty(*currentChild);
				*currentChild = NULL;
				currentChild ++;
			}
		}
		if (t == m_root) {
			t->m_children.clear();
		} else {
			delete t;
			t = 0;
		}
	}
}


void treeData::getFieldData(
	treeNode * t,
	MVectorArray &r,
	MVectorArray &v,
	MDoubleArray &m,
	int & index
	){
	if (t != 0) {
		r.set(t->m_r,index); 	// position in worldspace
		v.set(t->m_v,index);		// velocity temp in worldspace
		// v.set(t->m_a,index);		// acceleration temp in worldspace
		m.set(t->m_mass,index);
		index++;
		if (t->m_children.size()){
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				getFieldData( *currentChild, r, v, m, index);
				currentChild ++;
			}
		}
	}
}


void treeData::storeAppliedForce(treeNode * t, const MVectorArray &appliedForce, int & index){
	if (t != 0) {
		double segLen = (MVector(t->m_restMatrix(3,0), t->m_restMatrix(3,1), t->m_restMatrix(3,2))).length();
		t->m_appliedForce = appliedForce[index] * segLen;
		index++;
		if (t->m_children.size()){
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				storeAppliedForce( *currentChild, appliedForce, index);
				currentChild ++;
			}
		}
	}
}

void treeData::clearForces(treeNode * t){
	if (t != 0) {
		t->m_appliedForce = MVector::zero;
		t->m_distributedForce = MVector::zero;
		if (t->m_children.size()){
			//t->m_distributedTorque = MVector::zero;
			//t->m_springTorque = MVector::zero;
			//t->m_inertiaTorque = MVector::zero;
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				clearForces( *currentChild );
				currentChild ++;
			}
		}
	}
}


void  treeData::distributeForce(treeNode * t){
// yet another new dist	 force func where a node distributes the force it registers evenly among all it's parents
	if (t != 0) {
		unsigned int cc = unsigned(t->m_children.size());
		//t->m_distributedForce += t->m_appliedForce / t->m_depth;
		t->m_distributedForce += t->m_appliedForce ;
		if (cc){
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				distributeForce( *currentChild);
				t->m_distributedForce += ((*currentChild)->m_distributedForce) ;
				currentChild ++;
			}
			// not sure whther I should do this
			// t->m_distributedForce /= cc;
		}
	}
}

MStatus treeData::prepAnimated( treeNode * t, const MObject & dagNode ){


	MStatus st;


	MMatrix rmat;
	st = getJointMat(dagNode, rmat);er;
	t->m_restMatrix = rmat; 
   // at this point - phi is wrong. 
   // it should be the rotation between matrix and rest_matrix

	MMatrix diffMat =   t->m_matrix * rmat.inverse();

	MVector    axis;
	double    theta;
	MQuaternion q;
	q=diffMat;
	q.getAxisAngle(axis, theta);

	t->m_phi = axis * theta;

	MFnIkJoint jointFn(dagNode, &st  );
	unsigned cc = jointFn.childCount();
   // get the number of actual joints
   // unsigned jc=0;
	MIntArray jointChildIndices;
	for (unsigned i = 0;i<cc;i++) {
		if (jointFn.child(i).hasFn(MFn::kJoint) ) {
			jointChildIndices.append(i);
		}
	}
	unsigned jc = jointChildIndices.length();

	MObject child;
   //if (!(cc == t->m_children.size())) return MS::kFailure;
	if (!(jc == t->m_children.size())) return MS::kFailure;

	LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
	int i = 0;
	while (currentChild != t->m_children.end()) {
      //child = jointFn.child(i);
		child = jointFn.child(jointChildIndices[i]);

		st = prepAnimated( *currentChild, child );
		if (st != MS::kSuccess)  return MS::kFailure;
		currentChild ++; i++;
	}

	return MS::kSuccess;
}


void treeData::update( treeNode * t,  const MMatrix &parentMatrix, const double & dt ) {
	
	// go through the whole process, and then blend with restMat
	
	MStatus st;
	//	unsigned int counter = 0;
	MString method("treeData::update");
	if (t != 0) {
		
		
		// find out where my parent has put me and update linear motion variables _r, _v, _a.
		/////////////////////////////////////////////////////////////
		MMatrix worldMatrix = t->m_matrix * parentMatrix;
		MVector r0 = t->m_r;
		MVector v0 = t->m_v;
		t->m_r = MVector(worldMatrix(3,0) , worldMatrix(3,1) ,worldMatrix(3,2) );
		t->m_v = ((t->m_r - r0) / dt);
		t->m_a = ((t->m_v - v0) / dt);
		/////////////////////////////////////////////////////////////
		
		// do I have children
		/////////////////////////////////////////////////////////////
		unsigned int cc = unsigned(t->m_children.size());
		if (cc){
			// gather all torques acting on me
			/////////////////////////////////////////////////////////////
			// MVector inertiaForce = -(t->m_a * t->m_inertia * t->m_I  );
			MVector inertiaForce = -(t->m_a * t->m_inertia * t->m_mass  );
			
			t->m_distributedTorque 	= MVector::zero;
			t->m_springTorque 		= MVector::zero;
			t->m_inertiaTorque 		= MVector::zero;
			
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				
				MVector localDistForce = ((*currentChild)->m_distributedForce * worldMatrix.inverse());  // bring into local space
				
				MVector localTensionForce = ( inertiaForce * worldMatrix.inverse() ); // bring into local space
				
				MVector cvec = MVector( (*currentChild)->m_matrix(3,0) , (*currentChild)->m_matrix(3,1) ,(*currentChild)->m_matrix(3,2) );
				t->m_distributedTorque += (  cvec  ^ localDistForce ) ;
				t->m_inertiaTorque += (  cvec  ^ localTensionForce ) ;
				
				currentChild++;
			}
			double len = t->m_phi.length();
			
			t->m_springTorque = -(t->m_spring *t->m_phi);
			
			MVector torqueSum = t->m_springTorque + t->m_inertiaTorque + t->m_distributedTorque;
			
			
			// integrate angular momentum 
			/////////////////////////////////////////////////////////////
			
			// t->m_alpha = torqueSum  * t->m_IRecip; 	// get angular acceleration from torque and inertia moment (M = I * alpha)
			if ( t->m_mass > 0.00000001)
			t->m_alpha = torqueSum  / t->m_mass; 	// get angular acceleration from torque and inertia moment (M = I * alpha)

			MVector deltaOmega = t->m_alpha * dt; 	// change in velocity
			t->m_omega = t->m_omega * t->m_conserve;  	// a cheat to damp the system a little
			t->m_omega += deltaOmega; 				// new velocity
			MVector deltaPhi = t->m_omega * dt; 		// change in rotation
			t->m_phi += deltaPhi;					// new rotation
			len = t->m_phi.length();			// magnitude of rotation
												/////////////////////////////////////////////////////////////
			len = len* t->m_weight;


			// bound the rotation to 360 in either direction
			/////////////////////////////////////////////////////////////
			
// 			if (len > PI_X2) {
// 				len = fmod(len , PI_X2);
// 				t->m_phi.normalize();
// 				t->m_phi *= len;
// 			}
			
			/////////////////////////////////////////////////////////////
			
			// update the matrix
			/////////////////////////////////////////////////////////////
			MQuaternion q;

			q.setAxisAngle(t->m_phi, len);
			
			t->m_matrix = q.asMatrix()*t->m_restMatrix;
			
			worldMatrix = t->m_matrix * parentMatrix;
			
			// now do the children
			/////////////////////////////////////////////////////////////
			currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				update( *currentChild, worldMatrix , dt);
				currentChild ++;
			}
		}
	}
}

void treeData::update( treeNode * t ) {
	
	// go through the whole process, and then blend with restMat
	
	MStatus st;
	//	unsigned int counter = 0;
	MString method("treeData::update");
	if (t != 0) {

		unsigned int cc =unsigned(t->m_children.size());
		if (cc){

			t->m_matrix = t->m_restMatrix  ;

			LIST_OF_TREE_NODES::iterator  currentChild = t->m_children.begin();

			while (currentChild != t->m_children.end()) {
				update( *currentChild);
				currentChild ++;
			}
		}
	}
}
void treeData::setRotations( treeNode * t, MVectorArray &v  ) {
	if (t != 0) {
		if (t->m_children.size()){  // we only do this for nodes with children

			MTX::RotationOrder order;
			double rotValue[3];

			MMatrix tmp =  t->m_scaleOrientation.asMatrix() * t->m_matrix * t->m_orientation.asMatrix()  ;

			MTX ttmp(tmp);
			ttmp.getRotation(rotValue, order);
			v.append(rotValue);
			LIST_OF_TREE_NODES::iterator currentChild;
			currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				setRotations( *currentChild, v);
				currentChild ++;
			}
		}
	}
}

void treeData::getLocatorData(
	treeNode * t,
	const MMatrix &parentMatrix,
	const MMatrix &rootParentMatrix,
	bool doSegments ,
	bool doRawForce ,
	bool doDistributedForce ,
	bool doDistributedTorque ,
	bool doSpringTorque ,
	bool doTensionTorque ,
	bool doVel ,
	bool doAcc ,
	bool doPhi ,
	bool doOmega ,
	bool doAlpha ,
	bool doMatrix ,
	MVectorArray & positions,
	MVectorArray & segments,
	MVectorArray & rawForce,
	MVectorArray & distForce,
	MVectorArray & distTorque,
	MVectorArray & springTorque,
	MVectorArray & inertiaTorque,
	MVectorArray & vel,
	MVectorArray & acc,
	MVectorArray & phi,
	MVectorArray & omega,
	MVectorArray & alpha,
	MVectorArray & restMatrixX,
	MVectorArray & restMatrixY,
	MVectorArray & restMatrixZ,
	MVectorArray & matrixX,
	MVectorArray & matrixY,
	MVectorArray & matrixZ
	){
	if (t) {
		// cerr << " index -- "  << t->m_index << endl;
		
		MMatrix worldMatrix = t->m_matrix* parentMatrix;
		MMatrix invRootParentMat = rootParentMatrix.inverse();

		positions.append(  MVector( MPoint(t->m_r) * invRootParentMat   )) ;

		if (doSegments) {
			if (t->m_parent) {
				segments.append(( t->m_parent->m_r - t->m_r) * invRootParentMat );
			} else {
				segments.append(   (MVector(parentMatrix(3,0), parentMatrix(3,1),parentMatrix(3,2))     - t->m_r) * invRootParentMat) ;
			}
		}

		if (doRawForce) rawForce.append(t->m_appliedForce * invRootParentMat) ;
		if (doDistributedForce) distForce.append(t->m_distributedForce* invRootParentMat);
		if (doDistributedTorque) distTorque.append((t->m_distributedTorque * worldMatrix) *  invRootParentMat);
		if (doSpringTorque) springTorque.append((t->m_springTorque * worldMatrix)*  invRootParentMat);
		if (doTensionTorque) inertiaTorque.append((t->m_inertiaTorque * worldMatrix)*  invRootParentMat);
		if (doVel) vel.append(t->m_v*  invRootParentMat);
		if (doAcc) acc.append(t->m_a*  invRootParentMat);
		if (doPhi) phi.append((t->m_phi * worldMatrix)*  invRootParentMat);
		if (doOmega) omega.append((t->m_omega * worldMatrix)*  invRootParentMat);
		if (doAlpha) alpha.append((t->m_alpha * worldMatrix)*  invRootParentMat);
		if (doMatrix) {
			MMatrix restWorldMatrix =  (t->m_restMatrix * parentMatrix);
			restMatrixX.append((MVector::xAxis * restWorldMatrix)*  invRootParentMat);
			restMatrixY.append((MVector::yAxis * restWorldMatrix)*  invRootParentMat);
			restMatrixZ.append((MVector::zAxis * restWorldMatrix)*  invRootParentMat);
			matrixX.append((MVector::xAxis * worldMatrix)*  invRootParentMat);
			matrixY.append((MVector::yAxis * worldMatrix)*  invRootParentMat);
			matrixZ.append((MVector::zAxis * worldMatrix)*  invRootParentMat);
		}
		if (t->m_children.size()){
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();
			while (currentChild != t->m_children.end()) {
				getLocatorData(
					*currentChild, worldMatrix, rootParentMatrix, doSegments , doRawForce , doDistributedForce , doDistributedTorque , doSpringTorque ,
					doTensionTorque , doVel , doAcc , doPhi , doOmega , doAlpha , doMatrix ,
					positions,segments,rawForce,distForce,distTorque,springTorque,inertiaTorque,
					vel,acc,phi,omega,alpha,restMatrixX,restMatrixY,restMatrixZ,matrixX,matrixY,matrixZ
					);
				currentChild ++;
			}
		}
	}
}
void treeData::getEmitterData(treeNode * t,const MMatrix &parentMatrix,MVectorArray & positions,MVectorArray & vel,MVectorArray & acc,MVectorArray & rawForce){
	// get emitter data for all non terminal nodes
	if (t) {
		// cerr << " index -- "  << t->m_index << endl;
		MMatrix worldMatrix = t->m_matrix* parentMatrix;

		if (t->m_children.size()){
			positions.append(t->m_r);
			rawForce.append(t->m_appliedForce);
			vel.append(t->m_v);
			acc.append(t->m_a);
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();;
			while (currentChild != t->m_children.end()) {
				getEmitterData(*currentChild, worldMatrix, positions,vel,acc,rawForce);
				currentChild ++;
			}
		}
	}
}


void treeData::calcBoundingBox(treeNode * t, MPoint & minBB, MPoint & maxBB){
	if (t != 0) {

		MVector thePt = MPoint(t->m_r) *   m_parentMatrix.inverse();


		if (thePt.x < minBB.x ) minBB.x =thePt.x ;
		if (thePt.y < minBB.y ) minBB.y =thePt.y ;
		if (thePt.z < minBB.z ) minBB.z =thePt.z ;

		if (thePt.x > maxBB.x ) maxBB.x =thePt.x ;
		if (thePt.y > maxBB.y ) maxBB.y =thePt.y ;
		if (thePt.z > maxBB.z ) maxBB.z =thePt.z ;

		if (t->m_children.size()){
			LIST_OF_TREE_NODES::iterator currentChild = t->m_children.begin();;
			while (currentChild != t->m_children.end()) {
				calcBoundingBox(*currentChild,minBB, maxBB);
				currentChild ++;
			}
		}
	} else {
		minBB = MPoint::origin;
		maxBB = MPoint::origin;
	}
}
void  treeData::setBB(const MBoundingBox&  bb){
	m_BB =  bb;
}

MBoundingBox treeData::getBB(){
	return m_BB;
}
