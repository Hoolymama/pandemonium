/*
 * bendyBones.h
 * src
 * 
 * Created by Julian Mann on 14/02/2010.
 * 
 * Copyright (c) 2009 hooly
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of the project's author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
 

#ifndef  __bendyBones_H__
#define  __bendyBones_H__

#include <maya/MIOStream.h>
#include <vector>
#include <maya/MPxLocatorNode.h>
#include <maya/MDataBlock.h>
#include <maya/MFnDagNode.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>

#include <maya/MTime.h>
#include <maya/MVector.h>

#include <maya/MObjectArray.h>  
#include <maya/MFloatArray.h>  

#include <maya/MFnPluginData.h> 
#include "errorMacros.h"
#include "treeData.h"
#include "treeNode.h"


#define LEAD_COLOR        18  // green
#define ACTIVE_COLOR      15  // white
#define ACTIVE_AFFECTED_COLOR  8  // purple
#define DORMANT_COLOR      4  // blue
#define HILITE_COLOR      17  // pale blue
#define DORMANT_VERTEX_COLOR  8  // purple
#define ACTIVE_VERTEX_COLOR    16  // yellow

#define POINT_SIZE        2.0  


class bendyBones : public MPxLocatorNode
{
	
public:
	bendyBones();
	virtual ~bendyBones();
	
	static void    *creator();
	static MStatus  initialize();
	
	// virtual void postConstructor();
	virtual MStatus  compute( const MPlug& plug, MDataBlock& block );
	virtual void            draw( M3dView & view, const MDagPath & path,
								  M3dView::DisplayStyle style,
								  M3dView::DisplayStatus status );
	
	
	
	// Bounding box methods
	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 
	
	static MObject  bboxCorner1;
	static MObject  bboxCorner2;
	
	static MObject  aRootParentMatrix;
	
	
	static MObject aInRotation;
	static MObject aInRotationX;
	static MObject aInRotationY;
	static MObject aInRotationZ;
	
	
	static MObject  aSamplePoints;
	static MObject  aSampleVelocities;
	static MObject  aSampleDensities;
	static MObject  aSampleDeltaTime;
	static MObject  aSampleFieldData;
	
	static MObject  aEmitterPosition; 
	static MObject  aEmitterVelocity;   
	static MObject  aEmitterAcceleration; 
	static MObject  aEmitterForce; 
	static MObject  aEmitterData;
	
	static MObject  aInputForce;
	static MObject  aCurrentTime;
	static MObject  aStartTime;
	static MObject  aStaticTimePolicy;
	
	// static MObject  aAnimatable;
	
	static MObject  aEmission; 
	
	static MObject  aDynWeight; //
	static MObject  aFieldStrength; //
	
	
	static MObject  aSpring; // spring (spring constant) will be multiplied by cross section area
	
	
	// wood density will be multiplied by MOI to derive massMomentOfInertia
	// and multiplied by volume  to derive mass
	static MObject  aDensity; 
	
	// This is a multiplier for the torque produced by the fact that the base of the stick
	// is constrained to the stick below
	static MObject  aInertia;
	
	// conserve is the good old cheat as used for particles
	static MObject  aConserve;
	
	// cacheFile
	// static MObject  aUseCache;
	//static MObject  aCacheFile;
	//static MObject  aCacheFormat;
	
	// floats which multiply glyphs 
	
	
	static MObject  aShowSegments;
	
	static MObject  aShowRawForce;
	static MObject  aShowDistributedForce;
	static MObject  aShowDistributedTorque;
	static MObject  aShowSpringTorque;
	static MObject  aShowTensionTorque;
	
	static MObject  aShowVel;
	static MObject  aShowAcc;
	static MObject  aShowPhi;
	static MObject  aShowOmega;
	static MObject  aShowAlpha;
	static MObject  aShowMatrix; // will show both rest and current
	
	
	
	static MObject  aOutRotationX;
	static MObject  aOutRotationY;
	static MObject  aOutRotationZ;
	
	static MObject  aOutRotation;
	
	//static MObject  aDrawDummy;
	
	static MTypeId  id;
	
	// do all the tricky stuff!
	// MStatus workItOut( const MPlug& plug, MDataBlock& block, bool lazy=false );
	
	void jointCount(MObject & dagNode, int & numJoints);
	//MStatus assumeRestPose();
	//MStatus assumeRestPose(MDataBlock & block);
	// MStatus writeFrameCache(MString & result);
	MStatus updateCoefficients(MObject & rootJointNode, MDataBlock & block);
	MStatus updateCoefficients(MDataBlock & block);
	void doSimStep(MDataBlock &block,const MTime& dT);
	// void doSimStepOnCache(MDataBlock & block, const MTime& dT);
	//MStatus doCacheStep( const MTime& dT);
	//MStatus forceRestPoseCache();
	MStatus assumePreferredAngle();
	MStatus assumePreferredAngle(MObject & rootJointNode, MDataBlock & block);
	//MStatus assumeCachedRestPose(MDataBlock & block);
	
	
	MStatus fix() ;
	
	void doOutputs(MDataBlock & block);
	
	void doGLDraw(
				  M3dView & view,
				  const MVectorArray & points,
				  const MVectorArray & directions,
				  const double &mult, const MColor & color
				  );
	void doGLDraw(
				  M3dView & view,
				  const MVectorArray & points,
				  const MVectorArray & directions,
				  const double &mult
				  ); 
private:
   
   enum StaticTime {kNothing, kRestPose, kSimulate};
   
		MStatus getAppliedForces(
								 MDataBlock& block,
								 const MVectorArray &positions, 
								 const MVectorArray &velocities, 
								 const MDoubleArray &densities, 
								 const MTime &dT, 
								 MVectorArray &appliedForce
								 );
	// bendyBonesCache * m_cache;
	treeData * m_tree;
	MTime m_lastTimeIEvaluated;
	MTime timeValue( MDataBlock& data , MObject & attribute);
	
	
	int m_numJoints;
	// bool m_verbose;
	
};

inline MTime bendyBones::timeValue( MDataBlock& data, MObject & attribute )
{
	MStatus status;
	MDataHandle hValue = data.inputValue( attribute, &status );
	
	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();
	
	return( value );
}
#endif
