/*
* bendyBones.cpp
* src
* 
* Created by Julian Mann on 14/02/2010.
* 
* Copyright (c) 2009 hooly
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
* 
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
* 
* Neither the name of the project's author nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#include <math.h>

#include <maya/MGlobal.h>
#include <maya/MBoundingBox.h>
#include <maya/MAnimControl.h>
#include <maya/MDagPath.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnNurbsSurfaceData.h>
#include <maya/MItDag.h>
#include <maya/MColor.h>
#include <maya/MMatrix.h>
#include <maya/MPlugArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFnIkJoint.h>
#include <maya/MFileObject.h>

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif


#include "bendyBones.h"
#include "jMayaIds.h"

#define CACHE_BINARY  0
#define CACHE_ASCII    1

#define EMISSION_DATA_NONE  0
#define EMISSION_DATA_ALL  2

#define kNodeStateNormal		0
#define kNodeStateHasNoEffect	1

MObject bendyBones::bboxCorner1;
MObject bendyBones::bboxCorner2;

MObject bendyBones::aSamplePoints;
MObject bendyBones::aSampleVelocities;
MObject bendyBones::aSampleDensities;
MObject bendyBones::aSampleDeltaTime;
MObject bendyBones::aSampleFieldData;

MObject bendyBones::aEmitterPosition;
MObject bendyBones::aEmitterVelocity;
MObject bendyBones::aEmitterAcceleration;
MObject bendyBones::aEmitterForce;
MObject bendyBones::aEmitterData;

MObject bendyBones::aInputForce;

MObject bendyBones::aRootParentMatrix;

MObject bendyBones::aInRotationX;
MObject bendyBones::aInRotationY;
MObject bendyBones::aInRotationZ;
MObject bendyBones::aInRotation;



MObject  bendyBones::aEmission;
// MObject  bendyBones::aCreateLeaves;
MObject bendyBones::aDynWeight;
MObject bendyBones::aFieldStrength;
MObject bendyBones::aSpring;
MObject bendyBones::aDensity;
MObject bendyBones::aInertia;
MObject bendyBones::aConserve;


MObject bendyBones::aShowSegments;

MObject bendyBones::aShowRawForce;
MObject bendyBones::aShowDistributedForce;
MObject bendyBones::aShowDistributedTorque;
MObject bendyBones::aShowSpringTorque;
MObject bendyBones::aShowTensionTorque;

MObject bendyBones::aShowVel;
MObject bendyBones::aShowAcc;
MObject bendyBones::aShowPhi;
MObject bendyBones::aShowOmega;
MObject bendyBones::aShowAlpha;
MObject bendyBones::aShowMatrix;



MObject bendyBones::aOutRotationX;
MObject bendyBones::aOutRotationY;
MObject bendyBones::aOutRotationZ;
MObject bendyBones::aOutRotation;

MObject bendyBones::aCurrentTime;
MObject bendyBones::aStartTime;
MObject bendyBones::aStaticTimePolicy;
// MObject bendyBones::aDrawDummy;
MTypeId bendyBones::id( k_bendyBones );

bendyBones::bendyBones(){
	//m_cache = new bendyBonesCache;
	m_tree = new treeData(1);
	m_lastTimeIEvaluated = MAnimControl::currentTime();
	
	//m_verbose = false;
	m_numJoints = 0;
	
}
bendyBones::~bendyBones(){
	delete m_tree;
	// delete m_cache;
}


void *bendyBones::creator(){return new bendyBones;}

MStatus bendyBones::initialize()
{
	MStatus st;
	
	MString method("bendyBones::initialize");
	
	MFnNumericAttribute nAttr;
	MFnMessageAttribute mAttr;
	MFnTypedAttribute tAttr;
	MFnUnitAttribute uAttr;
	MFnCompoundAttribute cAttr;
	MFnMatrixAttribute matAttr;
	MFnEnumAttribute eAttr;
	
	
	
	bboxCorner1 = nAttr.create( "bboxCorner1", "bb1",  MFnNumericData::k3Double);
	nAttr.setKeyable(false);
	st = addAttribute( bboxCorner1 );er;
	
	bboxCorner2 = nAttr.create( "bboxCorner2", "bb2",  MFnNumericData::k3Double);
	nAttr.setKeyable(false);
	st = addAttribute( bboxCorner2 );er;
	
	
	///////////////////////////////////////////////////////////////////////
	aRootParentMatrix = matAttr.create("rootParentMatrix","rpmt");
	st = addAttribute( aRootParentMatrix );er;
	///////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////
	aInRotationX = uAttr.create("inRotationX","irox", MFnUnitAttribute::kAngle);
	aInRotationY = uAttr.create("inRotationY","iroy", MFnUnitAttribute::kAngle);
	aInRotationZ = uAttr.create("inRotationZ","iroz", MFnUnitAttribute::kAngle);
	aInRotation = nAttr.create("inRotation","irot",aInRotationX, aInRotationY, aInRotationZ);
	nAttr.setReadable( false );
	nAttr.setStorable( false );
	nAttr.setArray( true );
	// nAttr.setUsesArrayDataBuilder( true );
	st = addAttribute( aInRotation );er;
	///////////////////////////////////////////////////////////////////////
   
   
   // enum StaticTime {kNothing, kRestPose, kSimulate};
	

   aStaticTimePolicy = eAttr.create( "staticTimePolicy", "stp" );
   eAttr.addField("doNothing", bendyBones::kNothing);
   eAttr.addField("snapToRestPose",bendyBones::kRestPose );
   eAttr.addField("simulate",bendyBones::kSimulate );
   eAttr.setDefault(bendyBones::kSimulate );
   eAttr.setKeyable(true);
   eAttr.setHidden(false);
   st = addAttribute( aStaticTimePolicy );  er;
   
   
	aEmission = eAttr.create( "emission", "emi", EMISSION_DATA_NONE );
	eAttr.addField("off", EMISSION_DATA_NONE);
	eAttr.addField("on",EMISSION_DATA_ALL );
	eAttr.setKeyable(true);
	eAttr.setHidden(false);
	st = addAttribute( aEmission );  er;
	// Scalars for sim
	///////////////////////////////////////////////////////////////////////
	aDynWeight = nAttr.create("dynamicsWeight","dwt", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0 );
	st = addAttribute( aDynWeight );er;
	
	aSpring = nAttr.create("springConstant","scon", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0 );
	st = addAttribute( aSpring );er;
	
	
	aDensity = nAttr.create("density","den", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0 );
	st = addAttribute( aDensity );er;
	
	
	aInertia = nAttr.create("inertia","inr", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault(0.01);
	st = addAttribute( aInertia );er;
	
	aConserve = nAttr.create("conserve","con", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0 );
	st = addAttribute( aConserve );er;
	
	aFieldStrength = nAttr.create("fieldStrength","fst", MFnNumericData::kDouble);
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0 );
	st = addAttribute( aFieldStrength );er;
	
	///////////////////////////////////////////////////////////////////////
	
	
	
	
	aShowSegments = nAttr.create("showSegments","sseg", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.95 );
	st = addAttribute(aShowSegments);er;
	
	aShowRawForce = nAttr.create("showRawForce","sraw", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowRawForce);er;
	
	aShowDistributedForce = nAttr.create("showDistributedForce","sdis", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowDistributedForce);er;
	
	aShowDistributedTorque = nAttr.create("showDistributedTorque","sdtq", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowDistributedTorque);er;
	
	aShowSpringTorque = nAttr.create("showSpringTorque","sstq", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowSpringTorque);er;
	
	aShowTensionTorque = nAttr.create("showTensionTorque","sttq", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowTensionTorque);er;
	
	aShowVel = nAttr.create("showVel","svel", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowVel);er;
	
	aShowAcc = nAttr.create("showAcc","sacc", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowAcc);er;
	
	aShowPhi = nAttr.create("showPhi","sphi", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowPhi);er;
	
	aShowOmega = nAttr.create("showOmega","somg", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowOmega);er;
	
	aShowAlpha = nAttr.create("showAlpha","salp", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowAlpha);er;
	
	aShowMatrix = nAttr.create("showMatrix","smat", MFnNumericData::kDouble);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0);
	st = addAttribute(aShowMatrix);er;
	
	
	
	// Joint Rotation output
	///////////////////////////////////////////////////////////////////////
	aOutRotationX = uAttr.create("outRotationX","orox", MFnUnitAttribute::kAngle);
	aOutRotationY = uAttr.create("outRotationY","oroy", MFnUnitAttribute::kAngle);
	aOutRotationZ = uAttr.create("outRotationZ","oroz", MFnUnitAttribute::kAngle);
	aOutRotation = nAttr.create("outRotation","orot",aOutRotationX, aOutRotationY, aOutRotationZ);
	nAttr.setReadable( true );
	nAttr.setStorable( false );
	nAttr.setArray( true );
	nAttr.setUsesArrayDataBuilder( true );
	
	st = addAttribute( aOutRotation );er;
	
	
	// Time
	///////////////////////////////////////////////////////////////////////
	aCurrentTime = uAttr.create( "currentTime", "ct", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	st =  addAttribute(aCurrentTime);  er;
	
	aStartTime = uAttr.create( "startTime", "st", MFnUnitAttribute::kTime );
	st =  addAttribute(aStartTime);  er;
	

	///////////////////////////////////////////////////////////////////////
	
	
	// Attributes which are used to get the applied force values
	///////////////////////////////////////////////////////////////////////
	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray, &st);er;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aSamplePoints );er;
	
	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray, &st);er;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aSampleVelocities );er;
	
	aSampleDensities = tAttr.create("sampleDensities", "smss", MFnData::kDoubleArray, &st);er;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aSampleDensities );er;
	
	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0, &st ); er;
	uAttr.setStorable(false);
	uAttr.setReadable(true);
	st = addAttribute(aSampleDeltaTime);er;
	
	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleDensities);
	cAttr.addChild(aSampleDeltaTime);
	st = addAttribute(aSampleFieldData);er;
	///////////////////////////////////////////////////////////////////////
	
	
	// Applied forces come back here
	///////////////////////////////////////////////////////////////////////
	aInputForce = tAttr.create("inputForce", "if", MFnData::kVectorArray, &st);er;
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	st = addAttribute( aInputForce ); er;
	///////////////////////////////////////////////////////////////////////
	
	
	// Emitter data
	aEmitterPosition = tAttr.create("emitterPosition","epos", MFnData::kVectorArray, &st ); er;
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	aEmitterVelocity = tAttr.create("emitterVelocity","evel",MFnData::kVectorArray, &st ); er;
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	aEmitterAcceleration = tAttr.create("emitterAcceleration","eacc",MFnData::kVectorArray, &st ); er;
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	aEmitterForce = tAttr.create("emitterForce","efrc",MFnData::kVectorArray, &st ); er;
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	
	aEmitterData = cAttr.create("emitterData","edat", &st ); er;
	cAttr.addChild(aEmitterPosition);
	cAttr.addChild(aEmitterVelocity);
	cAttr.addChild(aEmitterAcceleration);
	cAttr.addChild(aEmitterForce);
	
	st = addAttribute(aEmitterData);er;
	
	// FIELDS
	st = attributeAffects( aCurrentTime, aSamplePoints );er;
	st = attributeAffects( aCurrentTime, aSampleVelocities);er;
	st = attributeAffects( aCurrentTime, aSampleDensities);er;
	st = attributeAffects( aCurrentTime, aSampleDeltaTime);er;
	st = attributeAffects( aCurrentTime, aSampleFieldData);er;
	
	// EMITTER
	st = attributeAffects( aCurrentTime, aEmitterData);er;
	
	// OUTPUTS
	st = attributeAffects( aCurrentTime, aOutRotationX);er;
	st = attributeAffects( aCurrentTime, aOutRotationY);er;
	st = attributeAffects( aCurrentTime, aOutRotationZ);er;
	st = attributeAffects( aCurrentTime, aOutRotation);er;
	// st = attributeAffects( aCurrentTime, aDrawDummy);er;
	
	
	st = attributeAffects( aRootParentMatrix, aOutRotationX);er;
	st = attributeAffects( aRootParentMatrix, aOutRotationY);er;
	st = attributeAffects( aRootParentMatrix, aOutRotationZ);er;
	st = attributeAffects( aRootParentMatrix, aOutRotation);er;
	
	
	
	st = attributeAffects( aInRotation, aOutRotationX);er;
	st = attributeAffects( aInRotation, aOutRotationY);er;
	st = attributeAffects( aInRotation, aOutRotationZ);er;
	st = attributeAffects( aInRotation, aOutRotation);er;

	st = attributeAffects( aStaticTimePolicy, aOutRotationX);er;
	st = attributeAffects( aStaticTimePolicy, aOutRotationY);er;
	st = attributeAffects( aStaticTimePolicy, aOutRotationZ);er;
	st = attributeAffects( aStaticTimePolicy, aOutRotation);er;
	//  st = attributeAffects( aRootParentMatrix, aDrawDummy);er;
	
	return( MS::kSuccess );
	
}


void bendyBones::draw(M3dView & view, const MDagPath & path, M3dView::DisplayStyle style,  M3dView::DisplayStatus status ) {
   //cerr << "in draw "<<endl;
   MObject thisNode = thisMObject();
   //MPlug dummyPlug(thisNode, aDrawDummy );
   //int dummyVal;
   //dummyPlug.getValue(dummyVal);
   // Multipliers for glyph length
   ////////////////////////////////////////////////////////////////////
   double showSegmentsVal;
   MPlug showSegmentsPlug(thisNode, aShowSegments);
   showSegmentsPlug.getValue(showSegmentsVal);
   
   double showRawForceVal;
   MPlug showRawForcePlug(thisNode, aShowRawForce);
   showRawForcePlug.getValue(showRawForceVal);
   
   double showDistributedForceVal;
   MPlug showDistributedForcePlug(thisNode, aShowDistributedForce);
   showDistributedForcePlug.getValue(showDistributedForceVal);
   
   double showDistributedTorqueVal;
   MPlug showDistributedTorquePlug(thisNode, aShowDistributedTorque);
   showDistributedTorquePlug.getValue(showDistributedTorqueVal);
   
   double showSpringTorqueVal;
   MPlug showSpringTorquePlug(thisNode, aShowSpringTorque);
   showSpringTorquePlug.getValue(showSpringTorqueVal);
   
   double showTensionTorqueVal;
   MPlug showTensionTorquePlug(thisNode, aShowTensionTorque);
   showTensionTorquePlug.getValue(showTensionTorqueVal);
   
   double showVelVal;
   MPlug showVelPlug(thisNode, aShowVel);
   showVelPlug.getValue(showVelVal);
   
   double showAccVal;
   MPlug showAccPlug(thisNode, aShowAcc);
   showAccPlug.getValue(showAccVal);
   
   double showPhiVal;
   MPlug showPhiPlug(thisNode, aShowPhi);
   showPhiPlug.getValue(showPhiVal);
   
   double showOmegaVal;
   MPlug showOmegaPlug(thisNode, aShowOmega);
   showOmegaPlug.getValue(showOmegaVal);
   
   double showAlphaVal;
   MPlug showAlphaPlug(thisNode, aShowAlpha);
   showAlphaPlug.getValue(showAlphaVal);
   
   double showMatrixVal;
   MPlug showMatrixPlug(thisNode, aShowMatrix);
   showMatrixPlug.getValue(showMatrixVal);
   ////////////////////////////////////////////////////////////////////
   
   // if any drawing at all is to be done then we have to make all the arrays
   // however by setting the size increment we can guarantee that memory will
   // be used efficiently
   ////////////////////////////////////////////////////////////////////
   
   bool doSegments 			= (showSegmentsVal != 0);
   bool doRawForce 			= (showRawForceVal != 0);
   bool doDistributedForce 	= (showDistributedForceVal != 0);
   bool doDistributedTorque 	= (showDistributedTorqueVal != 0);
   bool doSpringTorque 		= (showSpringTorqueVal != 0);
   bool doTensionTorque		= (showTensionTorqueVal != 0);
   bool doVel 					= (showVelVal != 0);
   bool doAcc 					= (showAccVal != 0);
   bool doPhi 					= (showPhiVal != 0);
   bool doOmega 				= (showOmegaVal != 0);
   bool doAlpha 				= (showAlphaVal != 0);
   bool doMatrix 				= (showMatrixVal != 0);
   
   if (
      (doSegments) ||
      (doRawForce) ||
      (doDistributedForce) ||
      (doDistributedTorque) ||
      (doSpringTorque) ||
      (doTensionTorque) ||
      (doVel) ||
      (doAcc) ||
      (doPhi) ||
      (doOmega) ||
      (doAlpha) ||
      (doMatrix)
      ) {
      //int siz = m_tree->size();
     MVectorArray positions;
     MVectorArray segments;
     MDoubleArray radii;
     MVectorArray rawForce;
     MVectorArray distForce;
     MVectorArray distTorque;
     MVectorArray springTorque;
     MVectorArray inertiaTorque;
     MVectorArray vel;
     MVectorArray acc;
     MVectorArray phi;
     MVectorArray omega;
     MVectorArray alpha;
     MVectorArray restMatrixX;
     MVectorArray restMatrixY;
     MVectorArray restMatrixZ;
     MVectorArray matrixX;
     MVectorArray matrixY;
     MVectorArray matrixZ;
      
      // cerr << "_tree->size()  " << _tree->size() << endl;
      
      // 		segments.setSizeIncrement(siz);
      // 		rawForce.setSizeIncrement(siz);
      // 		distForce.setSizeIncrement(siz);
      // 		distTorque.setSizeIncrement(siz);
      // 		springTorque.setSizeIncrement(siz);
      // 		inertiaTorque.setSizeIncrement(siz);
      // 		vel.setSizeIncrement(siz);
      // 		acc.setSizeIncrement(siz);
      // 		phi.setSizeIncrement(siz);
      // 		omega.setSizeIncrement(siz);
      // 		alpha.setSizeIncrement(siz);
      // 		restMatrixX.setSizeIncrement(siz);
      // 		restMatrixY.setSizeIncrement(siz);
      // 		restMatrixZ.setSizeIncrement(siz);
      // 		matrixX.setSizeIncrement(siz);
      // 		matrixY.setSizeIncrement(siz);
      // 		matrixZ.setSizeIncrement(siz);
      
      
      m_tree->getLocatorData(
      m_tree->m_root,
      m_tree->parentMatrix(),
      m_tree->parentMatrix(),
      doSegments  	    ,
      doRawForce  	    ,
      doDistributedForce  ,
      doDistributedTorque ,
      doSpringTorque      ,
      doTensionTorque     ,
      doVel			    ,
      doAcc			    ,
      doPhi			    ,
      doOmega 		    ,
      doAlpha 		    ,
      doMatrix		    ,
      positions			,
      segments			,
      rawForce			,
      distForce			,
      distTorque			,
      springTorque		,
      inertiaTorque		,
      vel					,
      acc					,
      phi					,
      omega				,
      alpha				,
      restMatrixX			,
      restMatrixY			,
      restMatrixZ			,
      matrixX				,
      matrixY				,
      matrixZ
      );
      
      // now draw
      ////////////////////////////////////////////////////////////////////
      
      MColor colorRawForce( MColor::kRGB, 0.0, 0.0, 1.0 );// blue
      MColor colorDistributedForce( MColor::kRGB, 0.0, 1.0, 1.0 );// cyan
      MColor colorDistributedTorque( MColor::kRGB, 0.0, 0.5, 1.0 ); // turquoise
      MColor colorSpringTorque( MColor::kRGB, 0.5, 0.0, 0.5 ); // purple
      MColor colorTensionTorque( MColor::kRGB, 1.0, 0.5, 0.0 ); // orange
      MColor colorVel( MColor::kRGB, 0.0, 0.0, 0.5 );// dark blue
      MColor colorAcc( MColor::kRGB, 1.0, 1.0, 0.0 );// yellow
      MColor colorPhi( MColor::kRGB, 0.0, 1.0, 0.0 );// green
      MColor colorOmega( MColor::kRGB, 1.0,0.0,1.0 );// magenta
      MColor colorAlpha( MColor::kRGB, 1.0, 1.0, 1.0 );// white
      MColor colorRestMatrix( MColor::kRGB, 0.66, 0.66, 0.66 );//  light grey
      MColor colorMatrix( MColor::kRGB, 0.33, 0.33, 0.33 );	// dark grey
      
      if (doSegments ) doGLDraw(view, positions, segments, showSegmentsVal);
      if (doRawForce ) doGLDraw(view, positions, rawForce, showRawForceVal, colorRawForce);
      if (doDistributedForce ) doGLDraw(view, positions, distForce, showDistributedForceVal, colorDistributedForce);
      if (doDistributedTorque ) doGLDraw(view, positions, distTorque, showDistributedTorqueVal, colorDistributedTorque);
      if (doSpringTorque ) doGLDraw(view, positions, springTorque, showSpringTorqueVal, colorSpringTorque);
      if (doTensionTorque ) doGLDraw(view, positions,inertiaTorque , showTensionTorqueVal, colorTensionTorque);
      if (doVel ) doGLDraw(view, positions, vel, showVelVal, colorVel);
      if (doAcc ) doGLDraw(view, positions, acc, showAccVal, colorAcc);
      if (doPhi ) doGLDraw(view, positions, phi, showPhiVal, colorPhi);
      if (doOmega ) doGLDraw(view, positions, omega, showOmegaVal, colorOmega);
      if (doAlpha ) doGLDraw(view, positions, alpha, showAlphaVal, colorAlpha);
      if (doMatrix ) {
         doGLDraw(view, positions,restMatrixX , showMatrixVal, colorRestMatrix);
         doGLDraw(view, positions,restMatrixY, showMatrixVal, colorRestMatrix);
         doGLDraw(view, positions,restMatrixZ , showMatrixVal, colorRestMatrix);
         doGLDraw(view, positions,matrixX , showMatrixVal, colorMatrix);
         doGLDraw(view, positions,matrixY, showMatrixVal, colorMatrix);
         doGLDraw(view, positions,matrixZ, showMatrixVal, colorMatrix);
      }
   }
   ////////////////////////////////////////////////////////////////////
}

void bendyBones::doGLDraw(	M3dView & view,const MVectorArray & points,const MVectorArray & directions,const double &mult)
{
	unsigned pLen = points.length();
	// cerr << "pLen " << pLen << endl;
	// cerr << "mult " << mult << endl;
	
	if (mult) {
		view.beginGL();
		//view.setDrawColor( color );
		glBegin( GL_LINES );
		for ( unsigned i = 0; i < pLen; i++ ) {
			// cerr <<  points[i] << " --------- " << directions[i] << endl;
			glVertex3f(
						float(points[i].x) ,
						float(points[i].y) ,
						float(points[i].z) );
			
			glVertex3f(
						float( (directions[i].x * mult) + points[i].x),
						float( (directions[i].y * mult) + points[i].y),
						float( (directions[i].z * mult) + points[i].z));
		}
		glEnd();
		view.endGL();
	}
}

void bendyBones::doGLDraw(	M3dView & view,	const MVectorArray & points,	const MVectorArray & directions,	const double &mult, const MColor & color)
{
	unsigned pLen = points.length();
	// cerr << "pLen " << pLen << endl;
	// cerr << "mult " << mult << endl;
	
	if (mult) {
		view.beginGL();
		view.setDrawColor( color );
		glBegin( GL_LINES );
		for ( unsigned i = 0; i < pLen; i++ ) {
			// cerr <<  points[i] << " --------- " << directions[i] << endl;
			glVertex3f(
						float(points[i].x) ,
						float(points[i].y) ,
						float(points[i].z) );
			
			glVertex3f(
						float( (directions[i].x * mult) + points[i].x),
						float( (directions[i].y * mult) + points[i].y),
						float( (directions[i].z * mult) + points[i].z));
		}
		glEnd();
		view.endGL();
	}
}

bool bendyBones::isBounded() const
{
	return true;
}

MBoundingBox bendyBones::boundingBox() const
{
	return m_tree->getBB();
	
	
}


MStatus bendyBones::compute(const MPlug& plug, MDataBlock& block)
{
	
	MStatus st;
	
	MString method("bendyBones::compute");

	if(
		!(
		(plug == aOutRotationX)    ||
		(plug == aOutRotationY)    ||
		(plug == aOutRotationZ)    ||
		(plug == aOutRotation)  ||
		(plug == aEmitterPosition)  ||
		(plug == aEmitterVelocity)  ||
		(plug == aEmitterAcceleration)  ||
		(plug == aEmitterForce)    ||
		(plug == aEmitterData)
		
		)
		)
	{
		return( MS::kUnknownParameter );
	}
	
	MObject thisNode = thisMObject();
	//double dynWeight = block.inputValue(aDynWeight).asDouble();
	
	// time parameters
	////////////////////////////////////////////////////////////////
	MTime cT = timeValue( block, aCurrentTime );
	MTime sT = timeValue( block, aStartTime );
	MTime dT = cT - m_lastTimeIEvaluated;
	MTime oT = cT - sT;  // offset from start frame
	
	StaticTime timePolicy = StaticTime(block.inputValue(aStaticTimePolicy).asShort());
	// bool allowTimewarps = block.inputValue(aAllowTimewarps).asBool();
	m_lastTimeIEvaluated = cT;
	////////////////////////////////////////////////////////////////
	
	// 	set the tree parent Matrix to match the parent of the bones.
	////////////////////////////////////////////////////////////////
	MDataHandle hInputMat = block.inputValue( aRootParentMatrix, &st );er;
	MMatrix parentMat(hInputMat.asMatrix());
	m_tree->setParentMatrix(parentMat);
	////////////////////////////////////////////////////////////////
	
	// get all the input rotations so the plugs trigger this compute - nothing else
	////////////////////////////////////////////////////////////////
	MArrayDataHandle hInRotation = block.inputArrayValue( aInRotation, &st );er;
	unsigned num = hInRotation.elementCount();
	for(unsigned i = 0;i < num; i++, hInRotation.next()) {
		MDataHandle hRot = hInRotation.inputValue(&st ); er;	
		MVector irot = hRot.asVector();
	}
	////////////////////////////////////////////////////////////////
	
	// 	Root
	////////////////////////////////////////////////////////////////
	MObject	rootNode;
	MPlugArray plugArray;
	MPlug rootPlug(thisNode, aRootParentMatrix );
	
	if(rootPlug.connectedTo(plugArray,1,0)) {
		rootNode = plugArray[0].node(&st); er;
	} else {
		return( MS::kUnknownParameter );
	}
	////////////////////////////////////////////////////////////////
	
	// have we got joints and a valid tree already?
	////////////////////////////////////////////////////////////////
	m_numJoints = 0;
	jointCount(rootNode, m_numJoints);
	bool validTree = (m_tree->size() > 1);
	////////////////////////////////////////////////////////////////



	// a simple way to ignore bendys but still update the bones is to use builtin state enum attribute
	////////////////////////////////////////////////////////////////
	short int nodeState = block.inputValue( state).asShort();
	if (nodeState == kNodeStateHasNoEffect)  {
		// kNodeStateHasNoEffect
		// so make the bendyBones do exactly what the source is doing
		st =  m_tree->updateRestMatrix(m_tree->m_root, rootNode);
		m_tree->update(m_tree->m_root);
		doOutputs(block);
		return( MS::kSuccess );
	}
	////////////////////////////////////////////////////////////////
	
	


	// 
	////////////////////////////////////////////////////////////////
	if (oT <= MTime(0.0)) {
		st = assumePreferredAngle(rootNode, block); // change to assume start pose
		doOutputs(block);
		return( MS::kSuccess );
		
	} else { // oT > MTime(0.0)
		//cerr << "(oT > MTime(0.0)" << endl;
		
		if (!validTree) {
			doOutputs(block);
			return( MS::kSuccess );
		}
		
		// if (dT > MTime(0.0)) { 
		if (dT > MTime(0.0) || (timePolicy ==  bendyBones::kSimulate) ) { 

         
         if (dT == MTime(0.0) ) dT=MTime(1.0) ;
         
			if (validTree) {
				
				if (m_numJoints == m_tree->size()) {
					st = updateCoefficients(rootNode, block);
					
               		// recursive function 
               		m_tree->prepAnimated(m_tree->m_root,rootNode );
                
					// this is what makes it animatable
               // st =  m_tree->updateRestMatrixDyn(m_tree->m_root, rootNode);              
               //  st =  m_tree->updateRestMatrix(m_tree->m_root, rootNode);

				} 
				// else{
				//	st = updateCoefficients( block );
				// }
				
				doSimStep( block, dT);
				doOutputs(block);
				return( MS::kSuccess );
			} 
		} else { 	
         if (timePolicy ==  bendyBones::kRestPose) {
            st =  m_tree->updateRestMatrix(m_tree->m_root, rootNode);
            m_tree->update(m_tree->m_root);
         } 
         
			// BTW - if we do allow timewarps - it means that when time is zero or going backwards we just do nothing
			doOutputs(block);
			return( MS::kSuccess );
		}
		
	}

	
	doOutputs(block); // just in case
	return( MS::kSuccess );
}


void bendyBones::doSimStep( MDataBlock &block,const MTime& dT){
	MStatus st;
	

	double dt = dT.as(MTime::kSeconds);

	m_tree->clearForces(m_tree->m_root);
	
	bool bFoundForces = false;
	MPlug forcesPlug(thisMObject(), aInputForce);
	unsigned numForces = forcesPlug.numElements();
	for (unsigned i = 0;(i<numForces) && (!bFoundForces);i++){
		if (forcesPlug.elementByPhysicalIndex(i).isConnected()) {
			bFoundForces = true;
		}
	}
	
	
	if (bFoundForces) {	
		
		unsigned siz = m_tree->size();
		MVectorArray positions(siz);
		MVectorArray velocities(siz);
		MDoubleArray masses(siz);
		MVectorArray appliedForce(siz);
		int index = 0;
		
		m_tree->getFieldData(m_tree->m_root , positions , velocities , masses ,index);
		st = getAppliedForces(block, positions, velocities, masses, dT, appliedForce);
		if (st.error()) {cerr << "problem with getAppliedForces"; return;};
		
		
		double fieldMult = block.inputValue(aFieldStrength).asDouble();		
		for (unsigned i=0; i < siz; i++ ) {
			if (!(appliedForce[i].isEquivalent(MVector::zero))) appliedForce[i] *= fieldMult;
		}
		
		
		index = 0;
		m_tree->storeAppliedForce(m_tree->m_root, appliedForce, index);
		m_tree->distributeForce(m_tree->m_root);
	}
	
	m_tree->update(m_tree->m_root, m_tree->parentMatrix() , dt );
	//doOutputs(block);
}


void bendyBones::jointCount(MObject & dagNode, int & n){
	MStatus st;
	//unsigned int counter = 0;
	MString method("bendyBones::jointCount");
	
	MFnIkJoint jointFn(dagNode, &st);
	if (st != MS::kSuccess) return; // if this is not a joint then return
	n ++;
	int cc = jointFn.childCount();
	MObject child;
	if (cc) {
		for (int i = 0; i < cc; i++) {
			child = jointFn.child(i);
			jointCount( child, n);
		}
	}
}

// Force Accumulator procedure for gathering maya's force fields
MStatus bendyBones::getAppliedForces(
									MDataBlock& block,
									const MVectorArray &positions,
									const MVectorArray &velocities,
									const MDoubleArray &densities,
									const MTime &dT,
									MVectorArray &appliedForce
									){
	
	MStatus st;
	//unsigned int counter = 0;
	MString method("bendyBones::getAppliedForces");
	unsigned int siz = appliedForce.length();

	MDataHandle hSampleVelocities = hSampleFieldData.child(aSampleVelocities );
	MDataHandle hSampleDensities = hSampleFieldData.child(aSampleDensities );
	MDataHandle hDeltaTime = hSampleFieldData.child(aSampleDeltaTime );
	
	MFnVectorArrayData fnSamplePoints;
	MObject dSamplePoints = fnSamplePoints.create( positions, &st ); ;if (st.error()){return st;}
	MFnVectorArrayData fnSampleVelocities;
	MObject dSampleVelocities = fnSampleVelocities.create( velocities, &st ); ;if (st.error()){return st;}
	MFnDoubleArrayData fnSampleDensities;
	MObject dSampleDensities = fnSampleDensities.create( densities, &st ); ;if (st.error()){return st;}
	
	hSamplePoints.set(dSamplePoints);
	hSampleVelocities.set(dSampleVelocities);
	hSampleDensities.set(dSampleDensities);
	hDeltaTime.set(dT);
	
	block.setClean(aSamplePoints);
	block.setClean(aSampleVelocities);
	block.setClean(aSampleDensities);
	block.setClean(aSampleDeltaTime);
	block.setClean(aSampleFieldData);
	
	// force accumulator
	MPlug forcesPlug(thisMObject(), aInputForce);  //  force plug
	unsigned numForces = forcesPlug.numElements(&st);if (st.error()){return st;}
	if (numForces) {
		for (unsigned nf = 0;nf<numForces;nf++){
			MPlug tmpForcePlug = forcesPlug.elementByPhysicalIndex(nf,&st);if (st.error()){continue;}
			if (tmpForcePlug.isConnected()) {
				MObject tmpForceObject;
				st = tmpForcePlug.getValue(tmpForceObject); if (st.error()){continue;}
				MFnVectorArrayData tmpForceFn(tmpForceObject);
				MVectorArray tmpForce = tmpForceFn.array(&st);if (st.error()){continue;}
				if (tmpForce.length() == siz) {
					for (unsigned t=0; t < siz; t++ ) {
						appliedForce[t] += tmpForce[t];
					}
				}
			}
		}
		// magic division
		for (unsigned t=0; t < siz; t++ ) {
			appliedForce[t] *= .001;
		}
		
	}
	
	return MS::kSuccess;
}



MStatus bendyBones::fix(){
	return m_tree->fix(m_tree->m_root);
	
}



MStatus bendyBones::assumePreferredAngle(){
	MStatus st;
	//unsigned int counter = 0;
	MString method("bendyBones::assumePreferredAngle");
	
	MObject  rootNode;
	MObject thisNode = thisMObject();
	MPlugArray plugArray;
	MPlug rootPlug(thisNode, aRootParentMatrix );
	if(rootPlug.connectedTo(plugArray,1,0)) {
		rootNode = plugArray[0].node(&st); er;
	} else {
		// no root joint
		return( MS::kUnknownParameter );
	}
	
	MDataBlock  block = forceCache();
	st = assumePreferredAngle(rootNode, block);er;
	return st;
}

MStatus bendyBones::assumePreferredAngle(MObject & rootNode, MDataBlock & block){
	MStatus st;
	//  unsigned int counter = 0;
	MString method("bendyBones::assumePreferredAngle");
	// cerr << "setting rest pose" << endl;
	
	
	//   Parent Matrix
	////////////////////////////////////////////////////////////////
	MDataHandle hInputMat = block.inputValue( aRootParentMatrix, &st );ert;
	MMatrix parentMat(hInputMat.asMatrix());
	m_tree->setParentMatrix(parentMat);
	////////////////////////////////////////////////////////////////
	
	
	
	//  read the simulation coefficients parameters
	////////////////////////////////////////////////////////////////
	double density = block.inputValue(aDensity).asDouble();
	//double leafDensity = block.inputValue(aLeafDensity).asDouble();
	double spring = block.inputValue(aSpring).asDouble();
	//double leafSpring = block.inputValue(aLeafSpring).asDouble();
	double inertia = block.inputValue(aInertia).asDouble();
	double conserve = block.inputValue(aConserve).asDouble();
	double dynWeight = block.inputValue(aDynWeight).asDouble();
	
	// clean and rebuild the tree
	////////////////////////////////////////////////////////////////
	st = m_tree->init();ert;
	int id = 0;
	int depth = 0;
	MFnDagNode dagFn(rootNode);
	m_tree->build(rootNode, parentMat, id, depth, spring,density, inertia, conserve,dynWeight);
	// cerr << "JUST DONE BUILD " << m_tree->size() << endl;
	
	////////////////////////////////////////////////////////////////
	return MS::kSuccess;
}
MStatus bendyBones::updateCoefficients(MObject & rootNode, MDataBlock & block){
	MStatus st = MS::kSuccess;
	//  unsigned int counter = 0;
	MString method("bendyBones::updateCoefficients");
	
	double density = block.inputValue(aDensity).asDouble();
	
	double spring = block.inputValue(aSpring).asDouble();
	
	double inertia = block.inputValue(aInertia).asDouble();
	double conserve = block.inputValue(aConserve).asDouble();
	double dynWeight = block.inputValue(aDynWeight).asDouble();
	// double smoothThreshold = block.inputValue(aSmoothThreshold).asDouble();
	
	
	MFnIkJoint jointFn(rootNode, &st);
	if (st ==  MS::kSuccess) {
		st = m_tree->updateCoefficients(m_tree->m_root, jointFn, spring, density, inertia, conserve,dynWeight);
	} else {
		// cerr << "root not a joint" << endl;
	}
	
	return st;
}

// MStatus bendyBones::updateCoefficients(MDataBlock & block){
// 	MStatus st = MS::kSuccess;
// 	//  unsigned int counter = 0;
// 	MString method("bendyBones::updateCoefficients");
	
// 	//  read the simulation coefficients parameters
// 	////////////////////////////////////////////////////////////////
// 	double density = block.inputValue(aDensity).asDouble();
// 	double spring = block.inputValue(aSpring).asDouble();
// 	double inertia = block.inputValue(aInertia).asDouble();
// 	double conserve = block.inputValue(aConserve).asDouble();
// 	double dynWeight = block.inputValue(aDynWeight).asDouble();
// 	st = m_tree->updateCoefficients(m_tree->m_root, spring , density, inertia, conserve,dynWeight);
// 	return st;
// }

void bendyBones::doOutputs(MDataBlock & block) {
	MStatus st;
	//  unsigned int counter = 0;
	MString method("bendyBones::doOutputs");
	
	
	
	// Set the rotations for the joints
	////////////////////////////////////////////////////////////////////
	MArrayDataHandle     hOutput = block.outputArrayValue( aOutRotation, &st ); er;
	MArrayDataBuilder    bOutput = hOutput.builder();
	if (m_numJoints == 1){
		MDataHandle hOut = bOutput.addElement(0);
		hOut.set(0.0,0.0,0.0);
	} else {
		MVectorArray outRotation;
		m_tree->setRotations(m_tree->m_root ,outRotation);
		unsigned len = outRotation.length();
		for  ( unsigned i = 0; i < len; i++) {
			MDataHandle hOut = bOutput.addElement(i);
			double3 &out = hOut.asDouble3();
			out[0] = outRotation[i].x;
			out[1] = outRotation[i].y;
			out[2] = outRotation[i].z;
		}
	}
	hOutput.setAllClean();
	
	////////////////////////////////////////////////////////////////////
	// emission data
	////////////////////////////////////////////////////////////////////
	// MDataHandle hDoEmission = block.inputValue( aEmission, &st ); er;
	short doEmission =  block.inputValue(aEmission).asShort();
	
	MDataHandle hOutEmitterData =   block.outputValue( aEmitterData, &st );er;
	MDataHandle hOutEPosition =   hOutEmitterData.child(aEmitterPosition);
	MDataHandle hOutEVelocity =   hOutEmitterData.child(aEmitterVelocity);
	MDataHandle hOutEAcceleration = hOutEmitterData.child(aEmitterAcceleration);
	MDataHandle hOutEForce =     hOutEmitterData.child(aEmitterForce);
	
	MVectorArray points;
	MVectorArray velocities;
	MVectorArray accels;
	MVectorArray forces;	
	
	if (doEmission)
	{
		m_tree->getEmitterData(m_tree->m_root, m_tree->parentMatrix(),points,velocities,accels,forces);
		
	}		
	MFnVectorArrayData fnEPoints;
	MFnVectorArrayData fnEVelocities;
	MFnVectorArrayData fnEAccels;
	MFnVectorArrayData fnEForces;
	
	MObject dEPoints =       fnEPoints.create(points , &st ); er;
	MObject dEVelocities =     fnEVelocities.create(velocities , &st ); er;
	MObject dEAccels =       fnEAccels.create(accels , &st ); er;
	MObject dEForces =       fnEForces.create(forces , &st ); er;
	
	hOutEPosition.set( dEPoints );
	hOutEVelocity.set( dEVelocities );
	hOutEAcceleration.set( dEAccels );
	hOutEForce.set( dEForces );
	
	
	hOutEmitterData.setClean( );
	
	
	MPoint minBB(10000000, 10000000, 10000000);
	MPoint maxBB(-10000000, -10000000, -10000000);
	m_tree->calcBoundingBox(m_tree->m_root, minBB,maxBB );
	m_tree->setBB(MBoundingBox(minBB,maxBB));
	
}

